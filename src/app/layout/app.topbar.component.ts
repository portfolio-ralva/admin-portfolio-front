import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import { MenuItem } from 'primeng/api';
import { LayoutService } from "./service/app.layout.service";
import { Router } from '@angular/router';
import {IUser} from "../ralva/components/auth/user";

@Component({
    selector: 'app-topbar',
    templateUrl: './app.topbar.component.html'
})
export class AppTopBarComponent implements OnInit{

    items!: MenuItem[];
  tieredItems: MenuItem[] = [];
  userAcces!: IUser;

    @ViewChild('menubutton') menuButton!: ElementRef;

    @ViewChild('topbarmenubutton') topbarMenuButton!: ElementRef;

    @ViewChild('topbarmenu') menu!: ElementRef;

    constructor(public layoutService: LayoutService,
                private router: Router) { }

  ngOnInit() {


    /*this.items = [
      {
        label: 'File',
        icon: 'pi pi-fw pi-file',
      },
      {
        label: 'Users',
        icon: 'pi pi-fw pi-user',
      },
      {
        label: 'Events',
        icon: 'pi pi-fw pi-calendar',
      },
      {
        separator: true
      },
      {
        label: 'Quit',
        icon: 'pi pi-fw pi-power-off',
        command: (event) => {this.logout()}
      }
    ];*/

    this.tieredItems = [
      /*{
        label: 'Customers',
        icon: 'pi pi-fw pi-table',
        items: [
          {
            label: 'New',
            icon: 'pi pi-fw pi-plus',
            items: [
              {
                label: 'Customer',
                icon: 'pi pi-fw pi-plus'
              },
              {
                label: 'Duplicate',
                icon: 'pi pi-fw pi-copy'
              },

            ]
          },
          {
            label: 'Edit',
            icon: 'pi pi-fw pi-user-edit'
          }
        ]
      },*/
      {
        label: 'Orders',
        icon: 'pi pi-fw pi-shopping-cart',
        items: [
          {
            label: 'View',
            icon: 'pi pi-fw pi-list'
          },
          {
            label: 'Search',
            icon: 'pi pi-fw pi-search'
          }

        ]
      },
      {
        label: 'Shipments',
        icon: 'pi pi-fw pi-envelope',
        items: [
          {
            label: 'Tracker',
            icon: 'pi pi-fw pi-compass',

          },
          {
            label: 'Map',
            icon: 'pi pi-fw pi-map-marker',

          },
          {
            label: 'Manage',
            icon: 'pi pi-fw pi-pencil'
          }
        ]
      },
      {
        label: 'Profile',
        icon: 'pi pi-fw pi-user',
        items: [
          {
            label: 'Settings',
            icon: 'pi pi-fw pi-cog'
          },
          {
            label: 'Billing',
            icon: 'pi pi-fw pi-file'
          }
        ]
      },
      { separator: true },
      {
        label: 'Quit',
        icon: 'pi pi-fw pi-sign-out',
        command: (event) => {this.logout()}
      }
    ];

    const userJson = localStorage.getItem('user');
    this.userAcces = userJson !== null ? JSON.parse(userJson): userJson;

    //this.userAcces = JSON.parse(localStorage.getItem("user"));
    console.log(
      'userrr', this.userAcces.name
    )
  }
  logout(){
      console.log("PRUEBA LOGOUT:::::::::::::::::::::::");
      localStorage.removeItem('token');
      localStorage.removeItem('user');
      this.router.navigate(["auth/login"]);
  }
}
