import { Injectable } from '@angular/core';
import {RaHostService} from "../../../../common/services/ra-host.service";
import {RaRestHttpService} from "../../../../common/services/ra-rest-http.service";
import {Router} from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user:any;
  token:any = '';
  constructor(
    private httpApiService: RaRestHttpService,
    private  hostApi: RaHostService,
    private router: Router,
  ) {
    this.loadStorage();
  }

  loadStorage(){
    if(localStorage.getItem("token")){
      this.token = localStorage.getItem("token");
      this.user = JSON.parse(localStorage.getItem("user") ?? '');
    }else{
      this.token = '';
      this.user = null;
    }
  }
  login(parametro:any): any {
    return this.httpApiService.post(
      this.hostApi.APPLICATION_API_HOST,
      '/login', parametro
    );
  }

  logout(){
    this.user = null;
    this.token = '';
    localStorage.removeItem('token');
    localStorage.removeItem('user');
    this.router.navigate(["auth/login"]);
  }

}
