import { Component } from '@angular/core';
import { LayoutService } from 'src/app/layout/service/app.layout.service';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {AuthService} from "../services/auth.service";
import {Message, MessageService} from "primeng/api";
import {Router} from "@angular/router";

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styles: [`
        :host ::ng-deep .pi-eye,
        :host ::ng-deep .pi-eye-slash {
            transform:scale(1.6);
            margin-right: 1rem;
            color: var(--primary-color) !important;
        }
    `],
  providers: [MessageService]
})
export class LoginComponent {

    valCheck: string[] = ['remember'];

    //password!: string;

  loginForm!: FormGroup;
  msgs: Message[] = [];
  user!: IUser;
  submitted = false;
  constructor(
    private service: MessageService,
    public layoutService: LayoutService,
    private authService: AuthService,
    public router:Router) {
    this.user = {} as IUser;


  }

  ngOnInit() {
    this.loginForm = new FormGroup({
      email: new FormControl(this.user.email, [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(250),
      ]),
      password: new FormControl(this.user.password, [
        Validators.required,
        Validators.minLength(8),
      ]),
    });

console.log(this.authService.user)
console.log(this.authService.token)

    if(this.authService.user && this.authService.token){
      this.router.navigate(["/"]);
    }
  }

  onSubmit() {
      if (this.loginForm.invalid) {
        for (const control of Object.keys(this.loginForm.controls)) {
          this.loginForm.controls[control].markAsTouched();
        }
        return;
      }

    this.user = this.loginForm.value;

    this.authService.login(this.user).subscribe((response: any) => {
      console.log('RESPONSE::::::::::::::::::',response);
      if (response.access_token) {
        if (response.access_token && response.user){
          localStorage.setItem("token", response.access_token);
          localStorage.setItem("user", JSON.stringify(response.user));
          console.log(true);
        }
      } else {
        console.log('IMPRIMIR ERROR');
        if (response.error.error === 'Unauthorized'){
          this.service.add({ key: 'tst', severity: 'error', summary: 'Error Message', detail: 'Acceso denegado, las credenciales no coinciden.' });
        }
      }
    });

  }

  get email() {
    return this.loginForm.get('email')!;
  }

  get password() {
    return this.loginForm.get('password')!;
  }
}

interface IUser {
  name: string;
  nickname: string;
  email: string;
  password: string;
  showPassword: boolean;
}
