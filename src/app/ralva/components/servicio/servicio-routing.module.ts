import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import {ServicioComponent} from "./servicio/servicio.component";
//import { IconsComponent } from './icons/icons.component';

@NgModule({
  imports: [RouterModule.forChild([
    { path: '', data: { breadcrumb: 'Servicios Brindados' }, component: ServicioComponent },
    { path: '**', redirectTo: '/notfound' }
  ])],
  exports: [RouterModule]
})
export class servicesRoutingModule { }
