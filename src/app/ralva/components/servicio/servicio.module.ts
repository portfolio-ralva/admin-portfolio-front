import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {InputTextModule} from "primeng/inputtext";
import {servicesRoutingModule} from "./servicio-routing.module";
import {ServicioComponent} from "./servicio/servicio.component";
import {DialogModule} from "primeng/dialog";
import {ToastModule} from "primeng/toast";
import {TableModule} from "primeng/table";
import {ToolbarModule} from "primeng/toolbar";
import {FileUploadModule} from "primeng/fileupload";
import {RatingModule} from "primeng/rating";
import {DropdownModule} from "primeng/dropdown";
import {RadioButtonModule} from "primeng/radiobutton";
import {InputNumberModule} from "primeng/inputnumber";
import {FormsModule} from "@angular/forms";

@NgModule({
  imports: [
    CommonModule,
    servicesRoutingModule,
    InputTextModule,
    DialogModule,
    ToastModule,
    TableModule,
    ToolbarModule,
    FileUploadModule,
    RatingModule,
    DropdownModule,
    RadioButtonModule,
    InputNumberModule,
    FormsModule
  ],
  declarations: [ServicioComponent]
})
export class ServicioModule { }
